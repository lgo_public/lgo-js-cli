'use strict';

const getOrderBook = require('./orderBook');

const weightedAveragePrice = (prices, quantity, direction) => {
  const levels = direction === 'BUY' ? prices.asks : prices.bids;
  let remainingQuantity = quantity;
  let totalAmount = 0;
  for (let i = 0; i < levels.length; i++) {
    const level = levels[i];
    if (level.quantity >= remainingQuantity) {
      totalAmount += remainingQuantity * level.price;
      remainingQuantity = 0;
      break;
    } else {
      remainingQuantity -= level.quantity;
      totalAmount += level.quantity * level.price;
    }
  }
  return Math.round((totalAmount / quantity) * 10) / 10;
};

const choosePriceByWap = (prices, direction, wap) => {
  if (direction === 'BUY') {
    let result = prices.asks[0].price;
    for (let i = 1; i < prices.asks.length; i++) {
      const level = prices.asks[i];
      if (level.price > wap) {
        break;
      }
      result = level.price;
    }
    return result.toFixed(1);
  } else {
    let result = prices.bids[0].price;
    for (let i = 1; i < prices.bids.length; i++) {
      const level = prices.bids[i];
      if (level.price < wap) {
        break;
      }
      result = level.price;
    }
    return result.toFixed(1);
  }
};

const getPricesByWap = (lgoClient, quantity, direction, productId) => {
  return getOrderBook(lgoClient, productId).then(prices => {
    const wap = weightedAveragePrice(prices, quantity, direction);
    const price = choosePriceByWap(prices, direction, wap);
    return { wap, price };
  });
};

module.exports = {
  getPricesByWap
};
