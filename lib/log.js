'use strict';
const chalk = require('chalk');

const log = message => console.log(chalk.yellow('#') + ' ' + message);

module.exports = log;
