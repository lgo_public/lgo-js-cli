'use strict';

const configuration = require('./configuration');
const balances = require('./balances');
const openOrders = require('./openOrders');
const cancel = require('./cancel');
const placeOrder = require('./placeOrder');
const wap = require('./wap');
const orderBook = require('./orderBook');
const commands = require('./commands');
const log = require('./log');
const util = require('./util');
const configurationCheck = require('./configurationCheck');

module.exports = {
  loadConfiguration: configuration.loadConfiguration,
  showBalances: balances,
  showOpenOrders: openOrders,
  cancel,
  commands,
  placeOrder,
  wap,
  getOrderBook: orderBook,
  log,
  util,
  configurationCheck
};
