'use strict';

const _ = require('lodash');
const log = require('./log');

const showOpenOrders = async (lgoClient, productId) =>
  lgoClient.getMyOpenOrders({ productId }).then(({ orders }) => {
    if (orders.length === 0) {
      log('No open order');
    } else {
      _.sortBy(orders, ['id']).forEach(order => {
        log(
          `${order.id}: ${order.productId} ${
            order.type === 'L' ? 'Limit' : 'Market'
          } ${order.side === 'B' ? 'buy' : 'sell'} $${order.price} rem.qty ${
            order.remainingQuantity
          }`
        );
      });
    }
  });

module.exports = showOpenOrders;
