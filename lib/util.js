'use strict';

const timeout = (timeoutInMs, resolvedData) => {
  let timeoutInstance;
  const result = new Promise(resolve => {
    timeoutInstance = setTimeout(() => {
      resolve(resolvedData);
    }, timeoutInMs);
  });
  return {
    promise: result,
    cancel: () => {
      clearTimeout(timeoutInstance);
    }
  };
};

module.exports = {
  timeout
};
