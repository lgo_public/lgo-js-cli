'use strict';

module.exports = {
  default: [
    {
      type: 'expand',
      name: 'command',
      message: 'Command',
      choices: [
        {
          key: 'b',
          name: 'Buy',
          value: 'buy'
        },
        {
          key: 's',
          name: 'Sell',
          value: 'sell'
        },
        {
          key: 'c',
          name: 'Cancel order',
          value: 'cancel'
        },
        {
          key: 'a',
          name: 'Cancel all open orders',
          value: 'cancel_all'
        },
        {
          key: 'o',
          name: 'Get open orders',
          value: 'open'
        },
        {
          key: 'f',
          name: 'Get available balances',
          value: 'balances'
        },
        {
          key: 'e',
          name: 'Exit',
          value: 'exit'
        }
      ]
    }
  ],
  cancelOrder: [
    {
      type: 'input',
      name: 'orderId',
      message: 'Order id',
      validate: function(value) {
        let valid = !isNaN(parseInt(value));
        return valid || 'Please enter a valid order id';
      }
    }
  ],
  confirmOrder: message => [
    {
      type: 'expand',
      name: 'confirm',
      message,
      choices: [
        {
          key: 'y',
          name: 'Yes',
          value: 'yes'
        },
        {
          key: 'n',
          name: 'No',
          value: 'no'
        },
        {
          key: 'r',
          name: 'reload',
          value: 'reload'
        }
      ]
    }
  ],
  quantity: [
    {
      type: 'input',
      name: 'quantity',
      message: 'Quantity',
      validate: function(value) {
        let valid = !isNaN(parseFloat(value));
        return valid || 'Please enter a number';
      }
    }
  ]
};
