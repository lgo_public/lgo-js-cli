'use strict';

const orderBook = async (lgoClient, productId) =>
  lgoClient.getOrderBookL2({ productId });

module.exports = orderBook;
