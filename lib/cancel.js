'use strict';
const log = require('./log');

const cancel = async (lgoClient, orderId) =>
  lgoClient
    .cancelOrder({ orderId, reference: Date.now() })
    .then(() => log(`Cancellation of ${orderId} sent`));

const cancelAll = async (lgoClient, productId) =>
  lgoClient
    .getMyOpenOrders({ productId })
    .then(({ orders }) =>
      orders.length === 0
        ? log('No order to cancel')
        : Promise.all(orders.map(o => cancel(lgoClient, o.id)))
    );

module.exports = { cancelOrder: cancel, cancelAll };
