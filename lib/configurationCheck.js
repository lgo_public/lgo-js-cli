'use strict';

const fs = require('fs');
const log = require('./log');

const check = async (lgoClient, configuration) => {
  if (['sandbox', 'production'].indexOf(configuration.environment) === -1) {
    log('LGO_ENV variable should contains "sandbox" or "production" value');
    return false;
  }
  if (!fs.existsSync(configuration.privateKeyPath)) {
    log('LGO_PRIVATE_KEY_PATH value is not a file');
    return false;
  }
  try {
    await lgoClient.getMyBalances();
  } catch (e) {
    log(JSON.stringify(e.data));
    log("Can't connect to the LGO API");
    return false;
  }
  return true;
};

module.exports = {
  check
};
