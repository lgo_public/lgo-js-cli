'use strict';

const log = require('./log');

const showBalances = async lgoClient =>
  lgoClient
    .getMyBalances()
    .then(data =>
      Object.entries(data).forEach(([key, value]) =>
        log(`${key}: ${value.available}, in order ${value.inOrder}`)
      )
    );

module.exports = showBalances;
