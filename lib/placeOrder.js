'use strict';

const { OrderType, OrderSide } = require('@lgo/sdk');
const log = require('./log');

const placeOrder = async (lgoClient, direction, quantity, price, productId) =>
  lgoClient
    .placeOrder({
      type: OrderType.limit,
      side: direction === 'BUY' ? OrderSide.buy : OrderSide.sell,
      productId,
      quantity,
      price,
      reference: Date.now()
    })
    .then(({ orderId }) => log(`Order placed: ${orderId}`));

module.exports = placeOrder;
