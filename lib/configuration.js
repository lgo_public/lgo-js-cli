'use strict';

const { loadConfiguration: load } = require('@arpinum/config');

const schema = {
  environment: {
    env: 'LGO_ENV',
    required: true
  },
  privateKeyPath: {
    env: 'LGO_PRIVATE_KEY_PATH',
    required: true
  },
  accessKey: {
    env: 'LGO_ACCESS_KEY',
    required: true
  },
  logLevel: {
    env: 'LGO_LOG_LEVEL',
    default: 'info'
  },
  logFile: {
    env: 'LGO_LOG_FILE',
    default: 'log.txt'
  },
  promptTimeout: {
    env: 'LGO_PROMPT_TIMEOUT',
    default: 60000
  },
  productId: {
    env: 'LGO_PRODUCT_ID',
    default: 'BTC-USD'
  }
};

function loadConfiguration() {
  return load(schema);
}

module.exports = { loadConfiguration };
