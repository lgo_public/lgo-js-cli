'use strict';
const { Client } = require('@lgo/sdk');
const { LocalRsaCryptoKi } = require('@lgo/sdk-localrsa');
const {
  loadConfiguration,
  showBalances,
  showOpenOrders,
  cancel,
  commands,
  placeOrder,
  wap,
  log,
  util,
  configurationCheck
} = require('./lib');
const inquirer = require('inquirer');
const configuration = loadConfiguration();

const myCryptoKi = new LocalRsaCryptoKi({
  privateKeyPath: configuration.privateKeyPath
});

const lgoClient = new Client({
  cryptoKi: myCryptoKi,
  accessKey: configuration.accessKey,
  env: configuration.environment
});

const confirmOrderWithTimeout = async (direction, quantity, price) => {
  const confirmPrompt = inquirer.prompt(
    commands.confirmOrder(`${direction} ${quantity} @ ${price}?`)
  );
  const timeout = util.timeout(configuration.promptTimeout, {
    confirm: 'no'
  });
  const { confirm } = await Promise.race([confirmPrompt, timeout.promise]);
  timeout.cancel();
  confirmPrompt.ui.close();
  return confirm;
};

const getPricesByWap = async (direction, quantity) => {
  const pricesByWap = await wap.getPricesByWap(
    lgoClient,
    quantity,
    direction,
    configuration.productId
  );
  log(`WAP: ${pricesByWap.wap}`);
  return pricesByWap;
};

const checkPriceAndPlaceOrder = async (direction, quantity) => {
  let pricesByWap;
  let confirm = 'reload';
  while (confirm === 'reload') {
    pricesByWap = await getPricesByWap(direction, quantity);
    confirm = await confirmOrderWithTimeout(
      direction,
      quantity,
      pricesByWap.price
    );
  }
  if (confirm === 'yes') {
    await placeOrder(
      lgoClient,
      direction,
      quantity,
      pricesByWap.price,
      configuration.productId
    );
  } else {
    log('Place order canceled');
  }
};

const buyOrSell = async direction => {
  const { quantity } = await inquirer.prompt(commands.quantity);
  await checkPriceAndPlaceOrder(direction.toUpperCase(), quantity);
};

const cancelOrder = async () => {
  const { orderId } = await inquirer.prompt(commands.cancelOrder);
  await cancel.cancelOrder(lgoClient, orderId);
};

const handlers = {
  buy: buyOrSell,
  sell: buyOrSell,
  cancel: cancelOrder,
  cancel_all: () => cancel.cancelAll(lgoClient, configuration.productId),
  open: () => showOpenOrders(lgoClient, configuration.productId),
  balances: () => showBalances(lgoClient)
};

const main = async () => {
  const configurationIsValid = await configurationCheck.check(
    lgoClient,
    configuration
  );
  if (configurationIsValid) {
    let answers = await inquirer.prompt(commands.default);
    while (answers.command !== 'exit') {
      await handlers[answers.command](answers.command);
      answers = await inquirer.prompt(commands.default);
    }
  } else {
    log('Please check your configuration');
  }
};

main();
