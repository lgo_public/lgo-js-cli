# LGO js-cli

## Generate rsa keypair

`openssl genrsa -out private_key.pem 2048`

`openssl rsa -in private_key.pem -pubout > public_key.crt`

## Install nvm

`curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh | bash`

## Install node

`nvm install 12.13.0`

## Install dependencies

`npm install`

## Run the app

`nvm use`

`LGO_ENV=sandbox LGO_ACCESS_KEY=0000000-2167-4057-a628-38c0e2ceed61 LGO_PRIVATE_KEY_PATH=/Users/bob/private_key.pem node index.js`

For production, use `LGO_ENV=production`.
